import json
from pathlib import Path

import pytest

from QompliGIS.__about__ import DIR_PLUGIN_ROOT_RESOURCES
from QompliGIS.report import LayerSection, MainSection, QompliGISReport, ReportFormat


def test_QompliGISReport():
    """
    Comparison of generated report with a reference
    """

    report = QompliGISReport()
    report.addLayer("layer 1")
    report.addFieldInfo("layer 1", "great field", "roughly correct")
    report.addLayerSection("layer 1", LayerSection.HOLES, "there are holes")
    report.addLayerSection("layer 1", LayerSection.DUPLICATES, "there are duplicates")
    report.addMainSection(MainSection.LAYER_LIST, "ok ok")
    report.addMainSection(MainSection.RESULT, "all is fine")
    with pytest.raises(ValueError) as e:
        report.addLayerSection("layer 1", LayerSection.FIELDS, "trial")
    assert str(e.value) == "For FIELDS section, use addFieldInfo() method"
    with pytest.raises(KeyError) as e:
        report.addLayerSection("layer 2", LayerSection.CRS, "trial")
    assert str(e.value) == "'The layer layer 2 is not present in the report'"
    assert (
        (
            '<head><link rel="stylesheet" href="'
            + str(DIR_PLUGIN_ROOT_RESOURCES)
            + '/css/report_html.css"><link rel="icon" type="image/png" href="'
            + str(DIR_PLUGIN_ROOT_RESOURCES)
            + '/images/qompligis_logo.png"><title>Report</title><meta charset="utf-8"></head>\
        <div id="logo"><img id="logo-oslandia" src="'
            + str(DIR_PLUGIN_ROOT_RESOURCES)
            + '/images/logo_oslandia.png">\
        <img id="logo-qompligis" src="'
            + str(DIR_PLUGIN_ROOT_RESOURCES)
            + '/images/qompligis_logo.png"></div>\
        <h1 id="title">Compliance Report</h1><ul class="nav"><li><a href="#layer 1">layer 1</a></li></ul><h1 id="Result">Result</h1><p>all is fine</p><h1 id="Layer list">Layer list</h1><p>ok ok</p><h1 id="layer 1">layer 1</h1><h2>Fields</h2><h3>great field</h3><p>roughly correct</p><h2>Holes</h2><p>there are holes</p><h2>Duplicate geometries</h2><p>there are duplicates</p>'
        )
        == report.report(ReportFormat.HTML)
    ), "HTML report don't match with reference"
    assert (
        Path(__file__).parent / "testdata" / "test_report.md.ref"
    ).read_text() == report.report(
        ReportFormat.MARKDOWN
    ), "MARKDOWN report don't match with reference"
    with open(Path(__file__).parent / "testdata" / "test_report.json.", "w") as f:
        f.write(report.report(ReportFormat.JSON))
    assert (
        Path(__file__).parent / "testdata" / "test_report.json.ref"
    ).read_text() == report.report(
        ReportFormat.JSON
    ), "JSON report don't match with reference"
