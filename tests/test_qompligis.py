"""
test QompliGIS
"""
from QompliGIS import classFactory


def test_qompligis(qgis_iface):
    """test qompligis.

    Just testing if clicking on the toolbar buttons does not crash

    :param qgis_iface:
    """
    qompligis = classFactory(qgis_iface)
    qompligis.initGui()
    qompligis.compliance_check_action.trigger()
    qompligis.edit_configuration_action.trigger()
    qompligis.create_configuration_action.trigger()
