"""
test structure comparisons
"""
import os
import tempfile
from collections import Counter

from QompliGIS.utils import (
    diff,
    dxf2gpkg,
    list_dxf_layers,
    list_gpkg_layers,
    list_shp_files,
)

DATAPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testdata")


def test_geopackage():
    """test geopackage."""
    gpkg_path = os.path.join(DATAPATH, "gpkg")
    orig = os.path.join(gpkg_path, "source.gpkg")
    comp = os.path.join(gpkg_path, "tocompare.gpkg")
    list_orig = list_gpkg_layers(orig)
    list_comp = list_gpkg_layers(comp)
    assert list_orig == ["autre_ouvrage", "branchement", "cana"]
    assert list_comp == ["autre_ouvrage", "branchement", "canalisation"]

    assert diff(list_orig, list_comp) == Counter({"canalisation": 1, "cana": -1})
    assert diff(list_comp, list_orig) == Counter({"cana": 1, "canalisation": -1})


def test_shp_folder():
    """test shp_folder."""
    shp_path = os.path.join(DATAPATH, "shp")
    orig = os.path.join(shp_path, "source")
    comp = os.path.join(shp_path, "tocompare")
    list_orig = list_shp_files(orig)
    list_comp = list_shp_files(comp)
    assert list_orig == ["autre_ouvrage", "branchement", "cana"]
    assert list_comp == ["autre_ouvrage", "branchement"]

    assert diff(list_orig, list_comp) == Counter({"cana": -1})
    assert diff(list_comp, list_orig) == Counter({"cana": 1})


def test_dxf():
    """test dxf."""
    gpkg_path = os.path.join(DATAPATH, "dxf")
    orig = os.path.join(gpkg_path, "source.dxf")
    comp = os.path.join(gpkg_path, "tocompare.dxf")
    list_orig = list_dxf_layers(orig)
    list_comp = list_dxf_layers(comp)
    assert list_orig == ["autre_ouvrage", "branchement", "cana"]
    assert list_comp == ["Calque1", "Cana"]

    assert diff(list_orig, list_comp) == Counter(
        {"Cana": 1, "Calque1": 1, "autre_ouvrage": -1, "cana": -1, "branchement": -1}
    )
    assert diff(list_comp, list_orig) == Counter(
        {"autre_ouvrage": 1, "cana": 1, "branchement": 1, "Cana": -1, "Calque1": -1}
    )


def test_dxf_to_gpkg():
    """test dxf_to_gpkg."""
    dxf_path = os.path.join(DATAPATH, "dxf")
    with tempfile.TemporaryDirectory() as tmp:
        comp = os.path.join(tmp, "dxf2gpkg.gpkg")
        dxf = os.path.join(dxf_path, "source.dxf")
        orig = os.path.join(dxf_path, "toGeoPackage.gpkg")
        list_orig = list_gpkg_layers(orig)

        dxf2gpkg(dxf, comp)

        list_comp = list_gpkg_layers(comp)

        assert diff(list_orig, list_comp) == Counter()
