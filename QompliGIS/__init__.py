"""
QompliGIS plugin
"""


def classFactory(iface):
    """Load the plugin class.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    from .qompligis import QompliGIS

    return QompliGIS(iface)
